import chain.Handler;
import chain.HandlerFactory;
import model.Context;

public class BotFacade {

    private final Handler handler = new HandlerFactory().getHandler();

    public void execute(String request) {
        Context context = handler.handle(request);
        context.getCommand().execute(context);
    }
}
