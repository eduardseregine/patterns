
public class Main {

    public static void main(String[] args) {
        BotFacade facade = new BotFacade();
        facade.execute("find");
        facade.execute("Tallinn");
        facade.execute("Narva");
        facade.execute("# tomorrow");
        facade.execute("/");
        facade.execute("my very complicated");
        facade.execute("no deal");
    }
}
