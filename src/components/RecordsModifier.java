package components;

public class RecordsModifier {

    public void changeTopic(String record, String param) {
        System.out.println("Topic changed to: " + record + " to " + param);
    }

    public void changeTime(String record, String param) {
        System.out.println("Time changed to: " + record + " to " + param);
    }

    public void changeText(String record, String param) {
        System.out.println("Text changed to: " + record + " to " + param);
    }

    public void changeDone(String record) {
        System.out.println("Record changed to done: " + record);
    }

    public void delete(String record) {
        System.out.println("Record deleted: " + record);
    }
}
