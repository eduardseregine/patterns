package components;

public class RecordSaverFinder {

    public void save(String text) {
        System.out.println("Saved: " + text);
    }

    public void find(String text) {
        System.out.println("Found records containing: " + text);
    }
}
