package command;

import model.Context;
import components.RecordSaverFinder;

public class RecordSaveCommand implements Command {

    RecordSaverFinder recordSaverFinder = new RecordSaverFinder();

    @Override
    public void execute(Context context) {
        recordSaverFinder.save(context.getParam());
    }
}
