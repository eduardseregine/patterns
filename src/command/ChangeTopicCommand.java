package command;

import model.Context;
import components.RecordsModifier;

public class ChangeTopicCommand implements Command {

    RecordsModifier recordsModifier = new RecordsModifier();

    @Override
    public void execute(Context context) {
        context.getRecords().forEach(x -> recordsModifier.changeTopic(x, context.getParam()));
    }
}
