package command;

import model.Context;
import components.RecordSaverFinder;

public class RecordFinderCommand implements Command {

    RecordSaverFinder recordSaverFinder = new RecordSaverFinder();

    @Override
    public void execute(Context context) {
        recordSaverFinder.find(context.getParam());
    }
}
