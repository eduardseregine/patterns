package command;

import model.Context;
import components.RecordsModifier;

public class DeleteCommand implements Command {

    RecordsModifier recordsModifier = new RecordsModifier();

    @Override
    public void execute(Context context) {
        context.getRecords().forEach(recordsModifier::delete);
    }
}
