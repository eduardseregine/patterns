package command;

import model.Context;

public class NoCommand implements Command {

    @Override
    public void execute(Context context) {
        System.out.println("Nothing has been requested");
    }
}
