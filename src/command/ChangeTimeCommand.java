package command;

import model.Context;
import components.RecordsModifier;

public class ChangeTimeCommand implements Command {

    RecordsModifier recordsModifier = new RecordsModifier();

    @Override
    public void execute(Context context) {
        context.getRecords().forEach(x -> recordsModifier.changeTime(x, context.getParam()));
    }
}
