package command;

import model.Context;
import components.Weather;

public class WeatherCommand implements Command {

    private final Weather weather = new Weather();

    @Override
    public void execute(Context context) {
            weather.showWeather(context.getParam());
    }
}
