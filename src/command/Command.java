package command;

import model.Context;

public interface Command {
    void execute(Context context);
}
