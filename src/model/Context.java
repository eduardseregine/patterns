package model;

import command.NoCommand;
import command.Command;

import java.util.List;

public class Context {

    Command command = new NoCommand();
    String param = "";
    List<String> records = List.of();

    public Command getCommand() {
        return command;
    }

    public String getParam() {
        return param;
    }

    public List<String> getRecords() {
        return records;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public void setRecords(List<String> records) {
        this.records = records;
    }
}
