package chain;

import command.ChangeTimeCommand;
import command.ChangeTopicCommand;
import command.DeleteCommand;
import command.DoneCommand;
import command.Command;
import model.Context;

import java.util.List;
import java.util.Map;

public class RecordsHandler extends BasicHandler implements Handler {

    private final Map<String, Command> commands = Map.of("+", new DoneCommand(),
            "/", new DeleteCommand(), "#", new ChangeTopicCommand(), "=", new ChangeTimeCommand());

    @Override
    protected boolean isCondition(String request) {
        return commands.keySet().stream().anyMatch(request::startsWith);
    }

    @Override
    protected Context handleByThis(String request) {
        Context context = new Context();
        context.setCommand(commands.get(String.valueOf(request.charAt(0))));
        context.setRecords(List.of("one", "two"));
        context.setParam(request.substring(1).trim());
        return context;
    }
}
