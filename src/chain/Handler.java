package chain;

import model.Context;

public interface Handler {

    void setNext(Handler handler);
    Context handle(String text);
}
