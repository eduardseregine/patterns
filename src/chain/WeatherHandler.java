package chain;

import command.WeatherCommand;
import command.Light;
import model.Context;

import java.util.List;

public class WeatherHandler extends BasicHandler implements Handler {

    private final List<String> cities = List.of("tallinn", "narva");

    @Override
    protected boolean isCondition(String request) {
        return cities.stream().anyMatch(x -> request.trim().equalsIgnoreCase(x));
    }

    @Override
    protected Context handleByThis(String request) {
        Context context = new Context();
        context.setCommand(new WeatherCommand());
        context.setParam(request);
        return context;
    }
}
