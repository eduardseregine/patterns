package chain;

import command.*;
import model.Context;


public class RecordsSaveHandler extends BasicHandler implements Handler {

    @Override
    protected boolean isCondition(String request) {
        return request.split(" ").length > 2;
    }

    @Override
    protected Context handleByThis(String request) {
        Context context = new Context();
        context.setCommand(new RecordSaveCommand());
        context.setParam(request);
        return context;
    }
}
