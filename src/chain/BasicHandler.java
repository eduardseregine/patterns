package chain;

import model.Context;

public abstract class BasicHandler {

    Handler next;

    public void setNext(Handler next) {
        this.next = next;
    }

    abstract protected boolean isCondition(String request);
    abstract protected Context handleByThis(String request);

    public Context handle(String request) {
        if (isCondition(request)) {
            return handleByThis(request);
        }
        if (next != null) {
            return next.handle(request);
        }
        return new Context();
    }
}
