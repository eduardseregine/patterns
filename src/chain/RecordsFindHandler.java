package chain;

import command.RecordFinderCommand;
import model.Context;

public class RecordsFindHandler extends BasicHandler implements Handler {

    @Override
    protected boolean isCondition(String request) {
        return request.split(" ").length == 1;
    }

    @Override
    protected Context handleByThis(String request) {
        Context context = new Context();
        context.setCommand(new RecordFinderCommand());
        context.setParam(request);
        return context;
    }
}
