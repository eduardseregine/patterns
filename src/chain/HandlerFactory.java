package chain;

import java.util.List;

public class HandlerFactory {

    private final List<Handler> handlerList = List.of(new RecordsHandler(), new WeatherHandler(),
            new RecordsFindHandler(), new RecordsSaveHandler());

    private Handler handler;

    public Handler getHandler() {
        prepare();
        return handler;
    }

    private void prepare() {
        if (handler == null) {
            handler = handlerList.get(0);
            for (int i = 0; i < handlerList.size() - 1; i++) {
                handlerList.get(i).setNext(handlerList.get(i + 1));
            }
        }
    }
}
